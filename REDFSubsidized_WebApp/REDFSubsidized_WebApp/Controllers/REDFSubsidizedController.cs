﻿using REDFSubsidized_WebApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace REDFSubsidized_WebApp.Controllers
{
    public class REDFSubsidizedController : Controller
    {
        // GET: REDFSubsidized
        public ActionResult Index()
        {
            return View();
        }
        public object InsertPaidRequest()
        {
           return REDFData.InsertPaidReq();
        }

        public object GetSupportRequest()
        {
            return REDFData.GetSupportReq();
        }
    }
}