﻿using REDFSubsidized_WebApp.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using REDFSubsidized_WebApp.Helper;
using System.Configuration;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net;
using System.Net.Security;
using System.Threading.Tasks;
using System.IO;
using REDFSubsidized_WebApp.Models;
using System.Globalization;

namespace REDFSubsidized_WebApp.Data
{
    public class REDFData
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        internal static object GetSupportReq()
        {

             DataTable data = SqlHelper.Select("LmsConnection", "GetREDFGetSupportDate", CommandType.StoredProcedure);
          
            foreach(DataRow row in data.Rows) {
                ProcessOneRecord(row);
            }
            return "success";

        }

        private static void ProcessOneRecord(DataRow row)
        {
            try
            {


                int ID = InsertREDFGetSupport(row["monthCol"].ToString(), row["Yearcol"].ToString());

                GetSupportReq.Envelope req = new GetSupportReq.Envelope()
                {
                    Body = new GetSupportReq.Body()
                    {
                        GetSupportPaid = new GetSupportReq.GetSupportPaid()
                        {
                            Credential = new GetSupportReq.Credential()
                            {
                                Password = ConfigurationManager.AppSettings["Password"],
                                UserName = ConfigurationManager.AppSettings["UserName"]
                            },
                            FMONTH = row["monthCol"].ToString(),
                            FYEAR = row["Yearcol"].ToString(),
                        }
                    }
                };
                string requestString = XMLParser<Data.GetSupportReq.Envelope>.ToWSDLGetSupport(req);
                log.Info("Request of post request : ");
                log.Info(requestString);
                log.Info("sending api request for month : " + row["monthCol"].ToString() + " , Year : " + row["Yearcol"].ToString());
                var result = GetSupportPostReq(requestString);
                log.Info(result);
                Data.GetSupportRs.Envelope responce;
                //Data.GetSupportRs.Envelope responce = testresult1();
               // var result = "testc";
                if (!string.IsNullOrEmpty(result?.ToString()))
                {
                   responce = XMLParser<Data.GetSupportRs.Envelope>.LoadFromXMLString(result.ToString());
                    log.Info(responce);
                    DataTable dataTable = GetSupportRsTable(ID, responce);
                    SaveGetSupportRsInDB(dataTable);
                }
            }catch(Exception ex)
            {
                log.Info(ex);
            }
        }

        private static int InsertREDFGetSupport(string v1, string v2)
        {
            SqlParameter[] Parameter = new SqlParameter[]
            {
                new SqlParameter("@Month", v1),
                new SqlParameter("@Year", v1)
        };

          var data=  SqlHelper.Scalar("LmsConnection", "InsertREDFGetSupportReq", CommandType.StoredProcedure, Parameter);
            if (data == null)
            {
                return -1;
            }
            return int.Parse(data.ToString());


        }

        private static void SaveGetSupportRsInDB(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                SqlParameter[] Parameter = new SqlParameter[1];
                SqlParameter dtParam = new SqlParameter("@REDFGetSupportRsType", SqlDbType.Structured);
                dtParam.Value = dt;
                Parameter[0] = dtParam;
                SqlHelper.Execute("LmsConnection", "InsertREDFGetSupportRs", CommandType.StoredProcedure, Parameter);

                //SaveRea
            }




        }
        private static DataTable GetSupportRsTable(int Id,GetSupportRs.Envelope responce)
        {
            DataTable dt = new DataTable();
            dt.Clear();
            dt.Columns.Add("AS_SERIAL");
            dt.Columns.Add("FULL_NAME_EN");
            dt.Columns.Add("IBAN");
            dt.Columns.Add("IBAN_BANK_NAME");
            dt.Columns.Add("NID");
            dt.Columns.Add("REPAY_NUM");
            dt.Columns.Add("SUPPORT_VALUE", System.Type.GetType("System.Decimal"));

            dt.Columns.Add("TRANSFER_APPROVAL_DATE_M");
            dt.Columns.Add("RequestId");
            foreach ( var t in responce.Body.GetSupportPaidResponse.GetSupportPaidResult.ResSuppPaidList.RealEstateSupportPaid)
            {
                DataRow _dr = dt.NewRow();
                _dr["AS_SERIAL"] = t.ASSERIAL;
                _dr["FULL_NAME_EN"] = t.FULLNAMEEN;
                _dr["IBAN"] = t.IBAN;
                _dr["IBAN_BANK_NAME"] = t.IBANBANKNAME;
                _dr["NID"] = t.NID;
                _dr["REPAY_NUM"] = t.REPAYNUM;
                _dr["SUPPORT_VALUE"] = t.SUPPORTVALUE;
                _dr["TRANSFER_APPROVAL_DATE_M"] = t.TRANSFERAPPROVALDATEM;
                _dr["RequestId"] = Id;
                
                dt.Rows.Add(_dr);

            }
            return dt;
        }


        public static object InsertPaidReq()
        {
            DataTable data = SqlHelper.Select("LmsConnection", "GetListForREDF", CommandType.StoredProcedure);
            // List<BillRecord_Type> BillRecord = new List<BillRecord_Type>();
            List<REDFClass> REDFList = DataTableToList(data);
            foreach (var rEDF in REDFList)
            {
                var item = GetEnvelope(rEDF);
                var accountNo = item?.Body?.InsertPaid?.Paid_List?.RealEstatePaid?.AS_SERIAL;
                int RequestId = InsertRequest(item);
                string requestString = XMLParser<Data.req.Envelope>.ToWSDL(item);
                log.Info("Request of post request : ");
                log.Info(requestString);
                log.Info("sending api request for MortgageAccountNumber " + accountNo);
                var result = postRequest(requestString);
                log.Info("Response of post request : ");
                log.Info(result);
                Data.Rs.Envelope responce;
                //Data.Rs.Envelope responce = testresult();
                // var result = "testc";
                if (!string.IsNullOrEmpty(result?.ToString()))
                {
                    responce = XMLParser<Data.Rs.Envelope>.LoadFromXMLString(result.ToString());
                    if (responce?.Body?.InsertPaidResponse?.InsertPaidResult != null)
                    {

                        string Description = "";
                        if (responce.Body?.InsertPaidResponse?.InsertPaidResult?.Ex_Description != null)
                        {
                            Description = responce.Body?.InsertPaidResponse?.InsertPaidResult?.Ex_Description;

                        }
                            var parameters = new[] {
                          new SqlParameter("@REDFTransactionId", rEDF.Id),
                            new SqlParameter("@RequestId",RequestId),
                            new SqlParameter("@Result",responce.Body?.InsertPaidResponse?.InsertPaidResult.Result),
                            new SqlParameter("@Value",responce.Body?.InsertPaidResponse?.InsertPaidResult.Value),
                            new SqlParameter("@ex_Description",Description),
                            new SqlParameter("@response",result)
                            };
                            log.Info("saving error response to database");
                            var success = SqlHelper.Execute("LmsConnection", "[Usp_InsertREDFInsertPaidRs]", CommandType.StoredProcedure, parameters) > 0;
                        if (success)
                        {
                            log.Info("Response saved in database for " + item?.Body?.InsertPaid?.Paid_List?.RealEstatePaid?.AS_SERIAL);
                        }
                        }

                    }

                
               
            }
            return "success";
        }

        private static Rs.Envelope testresult()
        {
            Data.Rs.Envelope responce = new Rs.Envelope()
            {
                Body = new Rs.Body
                {
                    InsertPaidResponse = new Rs.InsertPaidResponse()
                    {
                        InsertPaidResult = new Rs.InsertPaidResult()
                        {
                            Result = "test",
                            Value = "test",
                            Ex_Description = "test"
                        }
                    }
                }
            };
            return responce;
        }
        private static GetSupportRs.Envelope testresult1()
        {
            Data.GetSupportRs.Envelope responce = new GetSupportRs.Envelope()
            {
                Body = new GetSupportRs.Body
                {
                    GetSupportPaidResponse=new GetSupportRs.GetSupportPaidResponse()
                    {
                        GetSupportPaidResult=new GetSupportRs.GetSupportPaidResult()
                        {
                            ResSuppPaidList=new GetSupportRs.ResSuppPaidList()
                            {
                                RealEstateSupportPaid= new List<GetSupportRs.RealEstateSupportPaid>()
                                {
                                    new GetSupportRs.RealEstateSupportPaid()
                                    {
                                        FULLNAMEEN="sadasd",
                                        ASSERIAL="169994",
                                        IBAN="sa",
                                        SUPPORTVALUE=123.80,
                                        REPAYNUM=8,
                                        TRANSFERAPPROVALDATEM="as",
                                        NID="As"
                                    },
                                    new GetSupportRs.RealEstateSupportPaid()
                                    {
                                        FULLNAMEEN="sadasd1",
                                        ASSERIAL="169994",
                                        IBAN="sa",
                                        SUPPORTVALUE=123.80,
                                        REPAYNUM=9,
                                        TRANSFERAPPROVALDATEM="as",
                                        NID="As"
                                    },
                                    new GetSupportRs.RealEstateSupportPaid()
                                    {
                                        FULLNAMEEN="sadasd1",
                                        ASSERIAL="168583261",
                                        IBAN="sa",
                                        SUPPORTVALUE=123.80,
                                        REPAYNUM=15,
                                        TRANSFERAPPROVALDATEM="as",
                                        NID="As"
                                    },
                                     new GetSupportRs.RealEstateSupportPaid()
                                    {
                                        FULLNAMEEN="sadasd1",
                                        ASSERIAL="168583261",
                                        IBAN="sa",
                                        SUPPORTVALUE=123.80,
                                        REPAYNUM=16,
                                        TRANSFERAPPROVALDATEM="as",
                                        NID="As"
                                    }
                                }
                            }
                        }
                    }
                }
            };
            return responce;
        }

        public static object postRequest( string requestXml)
        {
            string destinationUrl = ConfigurationManager.AppSettings["PostUrl"];
            //string fileName = @"C:\Users\TareqAAlmokhadab\Downloads\test.xml";
            // byte[] file = File.ReadAllBytes(requestXml);
            byte[] file = System.Text.Encoding.Unicode.GetBytes(requestXml);

           
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            
            byte[] bytes = System.Text.Encoding.Unicode.GetBytes(requestXml);
            //  byte[] bytes = encoding.GetBytes(requestXml);

            //request.ContentType = "text/xml; encoding='gzip'";
            request.ContentType = "text/xml;";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            request.Headers.Add("Content-Encoding", "gzip");

            ServicePointManager.ServerCertificateValidationCallback = new
                                                 RemoteCertificateValidationCallback
                                                 (
                                                    delegate { return true; }
                                                 );
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;

            response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {

                Stream responseStream = response.GetResponseStream();

                string responseStr = new StreamReader(responseStream).ReadToEnd();

                return responseStr;
            }
            else
            {
                return response;
            }
            //return null;
        }
        public static object GetSupportPostReq(string requestXml)
        {
            string destinationUrl = ConfigurationManager.AppSettings["PostUrl"];
            //string fileName = @"C:\Users\TareqAAlmokhadab\Downloads\test.xml";
            // byte[] file = File.ReadAllBytes(requestXml);
            byte[] file = System.Text.Encoding.Unicode.GetBytes(requestXml);


            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);

            byte[] bytes = System.Text.Encoding.Unicode.GetBytes(requestXml);
            //  byte[] bytes = encoding.GetBytes(requestXml);

            //request.ContentType = "text/xml; encoding='gzip'";
            request.ContentType = "text/xml;";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            request.Headers.Add("Content-Encoding", "gzip");

            ServicePointManager.ServerCertificateValidationCallback = new
                                                 RemoteCertificateValidationCallback
                                                 (
                                                    delegate { return true; }
                                                 );
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;

            response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {

                Stream responseStream = response.GetResponseStream();

                string responseStr = new StreamReader(responseStream).ReadToEnd();

                return responseStr;
            }
            else
            {
                return response;
            }
            //return null;
        }

        private void PostAPiRequest()
        {

            int flag = 0, count = 0;
            string data = "";

            try
            {
                do
                {
                    if (count > 0)
                    {
                        log.Info("retry..." + count);
                    }
                    count++;

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["PostUrl"]);
                        ServicePointManager.ServerCertificateValidationCallback = new
                                                    RemoteCertificateValidationCallback
                                                    (
                                                       delegate { return true; }
                                                    );
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                        //client.SecurityProtocol = SecurityProtocolType.Tls12;
                        client.Timeout = TimeSpan.FromMinutes(20);
                        var uri = new Uri(string.Format("/id={0}&type={1}", "dfg", "sdf"));
                        var Send = client.GetAsync(uri).ContinueWith(task =>
                        {
                            if (task.Status == TaskStatus.RanToCompletion)
                            {
                                var responce = task.Result;
                                if (responce.IsSuccessStatusCode)
                                {
                                    flag = 1;
                                    data = responce.Content.ReadAsStringAsync().Result;
                                    log.Info("response" + data);
                                }
                            }

                        });
                        Send.Wait();
                    }
                } while (count <= 5 && flag == 0);
                //var status = InsertIntoSMSTrack(data, userID, messageTemplate);
            }
            catch (Exception ex)
            {
                var a = ex;
                log.Error(ex);
            }
        }

        private static int InsertRequest(req.Envelope item)
        {
            double PAY_AMOUNT = string.IsNullOrEmpty(item.Body.InsertPaid.Paid_List.RealEstatePaid.PAY_AMOUNT)?0:double.Parse(item.Body.InsertPaid.Paid_List.RealEstatePaid.PAY_AMOUNT);
            var parameter = new[] {
                new SqlParameter("@AS_SERIAL",item.Body.InsertPaid.Paid_List.RealEstatePaid.AS_SERIAL),
                new SqlParameter("@REPAY_NUM",item.Body.InsertPaid.Paid_List.RealEstatePaid.REPAY_NUM),

                new SqlParameter("@PAY_AMOUNT",PAY_AMOUNT),

                new SqlParameter("@PAY_DATE_M",item.Body.InsertPaid.Paid_List.RealEstatePaid.PAY_DATE_M),
                new SqlParameter("@NID",item.Body.InsertPaid.Paid_List.RealEstatePaid.NID)


                



                         };
            var data = SqlHelper.Scalar("LmsConnection", "Usp_InsertREDFInsertPaidReq", CommandType.StoredProcedure, parameter);
            if (data == null)
            {
                return -1;
            }
            return int.Parse(data.ToString());
        }

        private static List<REDFClass> DataTableToList(DataTable data)
        {
            //int i = 1;
            //string previouscontract = "";
            List<REDFClass> BillRecord = new List<REDFClass>();
            foreach (DataRow row in data.Rows)
            {
                //if (previouscontract != row["LMSReferenceNo"].ToString())
                //{
                //    i = 1;
                //    previouscontract = row["LMSReferenceNo"].ToString();
                //}
                //else
                //{
                //    i++;
                //}
                REDFClass rEDF = new REDFClass()
                {
                    Id=int.Parse(row["Id"]?.ToString()),
                    MortgageLoanAccountNumber = row["MortgageLoanAccountNumber"].ToString(),
                    NationalID = row["NationalID"].ToString(),
                    InstallmentAmount = row["InstallmentAmount"].ToString(),
                    TransactionDate = row["TransactionDate"].ToString(),
                    InstallmentNo = row["InstallmentNo"].ToString()
                };

                //Data.req.Envelope billRecord_Type = new Data.req.Envelope()
                //{

                //    Header = new req.Header()
                //    {
                //        Action = ConfigurationManager.AppSettings["ReqAction"]
                //    },
                //    Body = new req.Body()
                //    {
                //        InsertPaid = new req.InsertPaid()
                //        {
                //            Credential = new req.Credential()
                //            {
                //                Password = ConfigurationManager.AppSettings["Password"],
                //                UserName= ConfigurationManager.AppSettings["UserName"],


                //            },
                //            Paid_List= new req.Paid_List()
                //            {
                //                RealEstatePaid= new req.RealEstatePaid()
                //                {
                //                    AS_SERIAL= row["MortgageLoanAccountNumber"].ToString(),
                //                    NID= row["NIDTypeNameInAr"].ToString(),
                //                    PAY_AMOUNT= row["InstallmentAmount"].ToString(),
                //                    PAY_DATE_M= row["TransactionDate"].ToString(),
                //                    REPAY_NUM= row["InstallmentNo"].ToString()        //Which installment number is paid by customer
                //                }
                //            }

                //        }
                //    }

                //};
                BillRecord.Add(rEDF);

            }
            return BillRecord;
        }


        public static Data.req.Envelope GetEnvelope (REDFClass rEDF)
        {
            Data.req.Envelope billRecord_Type = new Data.req.Envelope()
            {

                Header = new req.Header()
                {
                    Action = ConfigurationManager.AppSettings["ReqAction"]
                },
                Body = new req.Body()
                {
                    InsertPaid = new req.InsertPaid()
                    {
                        Credential = new req.Credential()
                        {
                            Password = ConfigurationManager.AppSettings["Password"],
                            UserName = ConfigurationManager.AppSettings["UserName"],


                        },
                        Paid_List = new req.Paid_List()
                        {
                            RealEstatePaid = new req.RealEstatePaid()
                            {
                                AS_SERIAL = rEDF.MortgageLoanAccountNumber,
                                NID = rEDF.NationalID,
                                PAY_AMOUNT = rEDF.InstallmentAmount,
                                PAY_DATE_M = rEDF.TransactionDate,
                                REPAY_NUM = rEDF.InstallmentNo        //Which installment number is paid by customer
                            }
                        }

                    }
                }

            };

            return billRecord_Type;

        }
    }
}