﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace REDFSubsidized_WebApp.Models
{
    public class REDFClass
    {
        public int Id { get; set; }
        public string MortgageLoanAccountNumber { get; set; }
        public string NationalID { get; set; }
        public string InstallmentAmount { get; set; }
        public string TransactionDate { get; set; }
        public string InstallmentNo { get; set; }

    }
}